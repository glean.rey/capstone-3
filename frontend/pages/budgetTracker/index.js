import React, {useState, useContext, useEffect} from 'react'
import {Form, Button} from 'react-bootstrap'
import localStorage from 'localStorage'
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';
import Router from 'next/router';
import Head from 'next/head';
import { UserProvider } from '../../UserContext'
import moment from 'moment'

function index({trans, status}) {
    const [text, setText] = useState('')
    const [amount, setAmount] = useState('')
    const [history, setHistory] = useState(trans) 
    let accessToken = localStorage.getItem("token");

  if (status === 401) {
    return (
      <div className="text-center">
      <h1>Your session has time out please login</h1>
      <a href="./login">Login Here</a>
      </div>
    )
  }

  const [balance, setBalance] = useState(computeBalance())
  const [income, setIncome] = useState(computeIncome())
  const [expense, setExpense] = useState(computeExpense())

function computeBalance () {
  return computeIncome() + computeExpense()
} 

function computeIncome () {
  return history.reduce((total, item) => 
    (item.amount >= 0) ? total + item.amount : total, 0
  )
}

function computeExpense () {
  return history.reduce((total, item) => 
    (item.amount < 0) ? total + item.amount : 0, 0
  )
}

function onSubmit(e) {
  e.preventDefault();
  const options = {
    headers: {Authorization: `Bearer ${accessToken}`, 'Content-Type': 'application/json'},
    method: 'POST',
    body: JSON.stringify({text:text, amount:amount})
  }
  fetch (`${AppHelper.API_URL}/expenses`, options)
  .then(AppHelper.toJSON)
  .then(data=>{
    if(data) {
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Your work has been saved',
        showConfirmButton: false,
      })
      .then(() => {
        setAmount('')
        setText('')
        history.push({text:text, amount: Number(amount)})
        
          setHistory(history);
          console.log(history);
          Router.reload(window.location.budgetTracker);
        
      }) 
    }
    console.log(data);  
  })
}



//SEARCH BAR
const [term, setTerm] = useState('');
const [records, setRecords] = useState(history);
const [historyList, setHistoryList] = useState('');

useEffect(() => {

  if (term !== '') {
    let updatedHistory = records.map((histories) => {
      if (histories.text.toLowerCase().includes(term.toLowerCase())){
        return <histories key={histories.id} histories={histories}/>
      } else {
        return null
      }
    })

    setHistoryList(updatedHistory);

  } else {

    const updatedHistory = records.map(histories => {
      if(histories){
        return <histories key={histories.id} histories={histories}/>
      } else {
        return null;
      }
    })

    setHistoryList(updatedHistory);

  }

}, [term])
//END OF SEARCH BAR

  return (
    <UserProvider value={{accessToken: accessToken}}>
    <React.Fragment> 
      <Head>
        <title>Budget Tracker</title>
      </Head>
      <div className="container">
        <h4 className="text-center">Your Balance</h4>
        <h1 className="text-center">{moneyFormatter(balance)}</h1>
        <div className="inc-exp-container">
          <div>
            <h4>Income</h4>
            <p  className="money plus">+{moneyFormatter(income)}</p>
          </div>
          <div>
            <h4>Expense</h4>
            <p  className="money minus">-{moneyFormatter(expense)}</p>
          </div>
        </div>
      </div>
    
    <div>
      <h3 className="text-center">History</h3>
      {/* search bar */}
      <Form.Group controlId="search">
        <Form.Control 
          type="text" 
          placeholder="Search Here..."
          required
        />
      </Form.Group>
      {/* sort bar */}`
      <div className="btn-group">
      <button type="button" className="btn btn-success">Sort</button>
      <button type="button" className="btn btn-success dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
          <span className="sr-only">Toggle Dropdown</span>
      </button>
        <div className="dropdown-menu">
            <li>Income</li>
            <li>Expenses</li>
            <li>Recently Added</li>
        </div>
      </div>
    
      
      <ExpenseHistory history={history} />
      {/* <ul className="list">
      {history.map((transaction) => {
        return <li className={transaction.amount < 0 ? 'minus' : 'plus'}>{transaction.text}<span>{transaction.amount < 0 ? '-' : '+'}{moneyFormatter(transaction.amount)}</span><button type="button" className="delete-btn" onClick={(e) => onDelete(transaction._id)}>x</button></li>
      })}
      </ul> */}
    </div>
    <h3 className="text-center">New transaction</h3>
      <Form onSubmit={onSubmit}>
          <Form.Group>
            <Form.Label htmlFor="text">Title:</Form.Label>
            <Form.Control type="text" value={text} onChange={(e) => setText(e.target.value)} placeholder="Enter Title" required/>
          </Form.Group>
          <h3 className="text-center">Category</h3>
          <Form.Group>
            <Form.Label htmlFor="amount">Income:</Form.Label>
            <Form.Control type="number"  onChange={(e) => setAmount(e.target.value)} placeholder="Enter amount ₱"/>
          </Form.Group>
          <Form.Group>
            <Form.Label htmlFor="amount">Expense:<br />
            (Add negative sign before the number)</Form.Label>
            <Form.Control type="number"  onChange={(e) => setAmount(e.target.value)} placeholder="Enter amount - ₱"/>
          </Form.Group>
          <div className="text-center">
          <Button type="submit" className="btn" variant="primary">Add transaction</Button>
          </div>
        </Form>
    </React.Fragment>
    </UserProvider>
    )
}

index.getInitialProps = async({ req }) => {
    let result = []
    let accessToken = localStorage.getItem("token");
    const options = {
      headers: {Authorization: `Bearer ${accessToken}`, 'Content-Type': 'application/json'},
      method: 'GET',
    }
    const history = await fetch(`${AppHelper.API_URL}/expenses`, options) 
    const json = await history.json()
    
    if (json) result = json 
    
    return  {
      trans: result,
      status: history.status
    }
}

function moneyFormatter(num) {
  let p = num.toFixed(2).split('.');
  return (
    '₱' +
    p[0]
      .split('')
      .reverse()
      .reduce(function (acc, num, i, orig) {
        return num === '-' ? acc : num + (i && !(i % 3) ? ',' : '') + acc;
      }, '') +
    '.' +
    p[1]
  );
}

function onDelete(expenseId) {
  let accessToken = localStorage.getItem("token");
  const options = {
    headers: {Authorization: `Bearer ${accessToken}`, 'Content-Type': 'application/json'},
    method: 'DELETE',
  }
  console.log(expenseId)
  fetch (`${AppHelper.API_URL}/expenses/${expenseId}`, options)
  .then(AppHelper.toJSON)
  .then(data=>{
    console.log(data);
    if(data) {
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Your work has been deleted',
        showConfirmButton: false,
      })
      Router.reload(window.location.budgetTracker);
    } 
  })
}

function ExpenseHistory({ history }) {
  let date = history.createdOn
  return (
    <ul className="list">
      {history.map((transaction) => {
        return <li className={transaction.amount < 0 ? 'minus' : 'plus'}>{transaction.text}<span className="text-">{transaction.amount < 0 ? '-' : '+'}{moneyFormatter(transaction.amount)}<br/>{moment(date).format('MMMM-DD-YYYY')}</span><button type="button" className="delete-btn" onClick={(e) => onDelete(transaction._id)}>x</button></li>
      })}
    </ul>
  );
}

export default index