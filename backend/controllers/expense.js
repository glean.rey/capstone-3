const Expense = require('../models/Expense');

module.exports.track = (params, userId) => {

	let expense = new Expense({
		text: params.text,
        amount: params.amount,
		userId: userId
	})

	return expense.save().then((expense, err) => {

		return (err) ? false : true
	})

}

module.exports.get = (params) => {
	return Expense.findById(params.expenseId).then(expense => expense)
}

module.exports.getAll = (userId) => {
	return Expense.find({userId: userId}).then(expense => expense)
}

module.exports.delete = (params) => {
	return Expense.findByIdAndDelete(params.expenseId).then(expense => expense)
}